#include "raylib.h"
#include <cstdlib> // For std::rand() and std::srand()
#include <ctime>   // For std::time()

// window dimensions
const int windowDimensions[2]{512, 380};
// acceleration due to gravity (pixels/seconds)/seconds
const int gravity{1'000};
// amount of nebulae
const int sizeOfNebulae{10};
// nebula X velocity (pixels/seconds)
const int nebVel{-200};
// scarfy jump velocity (pixels/seconds)
const int jumpVel{-600};

struct AnimData
{
    Rectangle rec;
    Vector2 pos;
    int frame;
    int sizeOfFrames;
    float updateTime;
    float runningTime;
    Color tint;
};

// Function to generate a random color
Color GetRandomColor()
{
    return (Color){
        (unsigned char)std::rand() % 256, // Random R value
        (unsigned char)std::rand() % 256, // Random G value
        (unsigned char)std::rand() % 256, // Random B value
        255                               // Alpha value (fully opaque)
    };
}

bool isOnGround(AnimData data, int windowHeight)
{
    return data.pos.y >= windowHeight - data.rec.height;
}

AnimData updateAnimData(AnimData data, float deltaTime)
{
    data.runningTime += deltaTime;
    if (data.runningTime >= data.updateTime)
    {
        data.runningTime = 0.0f;

        data.rec.x = data.frame * data.rec.width;
        data.frame++;
        if (data.frame >= data.sizeOfFrames)
        {
            data.frame = 0;
        }
    }

    return data;
}

bool checkCollision(AnimData nebula, AnimData scarfy)
{
    float pad{50};
    Rectangle nebRec{
        nebula.pos.x + pad,
        nebula.pos.y + pad,
        nebula.rec.width - 2 * pad,
        nebula.rec.height - 2 * pad,
    };

    Rectangle scarfyRec{
        scarfy.pos.x,
        scarfy.pos.y,
        scarfy.rec.width,
        scarfy.rec.height,
    };

    return CheckCollisionRecs(nebRec, scarfyRec);
}

int main()
{
    // Initialize random seed
    std::srand(std::time(nullptr));

    // initialize the window
    InitWindow(windowDimensions[0], windowDimensions[1], "Dapper Dasher - MRWN Version");

    // nebula variables
    Texture2D nebula = LoadTexture("textures/12_nebula_spritesheet.png");
    const int sizeOfNebulaFrames{8};
    AnimData nebulae[sizeOfNebulae]{};
    for (int i = 0; i < sizeOfNebulae; i++)
    {
        nebulae[i].rec = {0.0f, 0.0f, nebula.width / 8.0f, nebula.height / 8.0f};
        nebulae[i].pos.x = windowDimensions[0] + i * 300.0f;
        nebulae[i].pos.y = windowDimensions[1] - nebula.height / 8.0f;
        nebulae[i].frame = 0;
        nebulae[i].sizeOfFrames = sizeOfNebulaFrames;
        nebulae[i].runningTime = 0.0f;
        nebulae[i].updateTime = 1.0f / 16.0f;
        nebulae[i].tint = GetRandomColor();
    }

    float finishLine{nebulae[sizeOfNebulae - 1].pos.x};

    // scarfy variables
    Texture2D scarfy = LoadTexture("textures/scarfy.png");
    AnimData scarfyData{
        {0.0f, 0.0f, scarfy.width / 6.0f, float(scarfy.height)},                                                // Rectangle rec
        {windowDimensions[0] / 2.0f - scarfy.width / 6.0f / 2.0f, windowDimensions[1] - scarfyData.rec.height}, // Vector2 pos
        int{},                                                                                                  // int frame
        int{6},                                                                                                 // int frame
        float{1.0f / 12.0f},                                                                                    // float updateTime
        float{},                                                                                                // float runningTime
    };

    // is the rectangle in the air
    bool isInAir{false};
    int velocity{};

    Texture2D background = LoadTexture("textures/far-buildings.png");
    float bgX{};
    Texture2D midground = LoadTexture("textures/back-buildings.png");
    float mgX{};
    Texture2D foreground = LoadTexture("textures/foreground.png");
    float fgX{};

    bool collision{};

    SetTargetFPS(60);
    while (!WindowShouldClose())
    {
        // delta time (time since last frame)
        const float deltaTime{GetFrameTime()};

        // start drawing
        BeginDrawing();
        ClearBackground(WHITE);

        // Scroll background
        bgX -= 20.0f * deltaTime;
        if (bgX <= -background.width * 2.0f)
        {
            bgX = 0.0f;
        }

        // Scroll midground
        mgX -= 40.0f * deltaTime;
        if (mgX <= -midground.width * 2.0f)
        {
            mgX = 0.0f;
        }

        // Scroll foreground
        fgX -= 80.0f * deltaTime;
        if (fgX <= -foreground.width * 2.0f)
        {
            fgX = 0.0f;
        }

        // draw background
        Vector2 bg1Pos{bgX, 0.0f};
        DrawTextureEx(background, bg1Pos, 0.0f, 2.0f, WHITE);
        Vector2 bg2Pos{bgX + background.width * 2.0f, 0.0f};
        DrawTextureEx(background, bg2Pos, 0.0f, 2.0f, WHITE);

        // draw midground
        Vector2 mg1Pos{mgX, 0.0f};
        DrawTextureEx(midground, mg1Pos, 0.0f, 2.0f, WHITE);
        Vector2 mg2Pos{mgX + midground.width * 2.0f, 0.0f};
        DrawTextureEx(midground, mg2Pos, 0.0f, 2.0f, WHITE);

        // draw foreground
        Vector2 fg1Pos{fgX, 0.0f};
        DrawTextureEx(foreground, fg1Pos, 0.0f, 2.0f, WHITE);
        Vector2 fg2Pos{fgX + foreground.width * 2.0f, 0.0f};
        DrawTextureEx(foreground, fg2Pos, 0.0f, 2.0f, WHITE);

        // perform ground check
        if (isOnGround(scarfyData, windowDimensions[1]))
        {
            // rectangle is on the ground
            velocity = 0;
            isInAir = false;
        }
        else
        {
            // rectangle is in the air
            // apply gravity
            velocity += gravity * deltaTime;
            isInAir = true;
        }

        // jump check
        if (IsKeyPressed(KEY_SPACE) && !isInAir)
        {
            velocity += jumpVel;
        }

        // update scarfy position
        scarfyData.pos.y += velocity * deltaTime;

        // update nebulae position
        for (int i = 0; i < sizeOfNebulae; i++)
        {
            nebulae[i].pos.x += nebVel * deltaTime;
        }

        // update scarfy animation frame
        if (!isInAir)
        {
            scarfyData = updateAnimData(scarfyData, deltaTime);
        }

        for (int i = 0; i < sizeOfNebulae; i++)
        {
            // update nebulae animation frame
            nebulae[i] = updateAnimData(nebulae[i], deltaTime);
        }

        for (AnimData nebData : nebulae)
        {
            // check collision
            if (checkCollision(nebData, scarfyData))
            {
                collision = true;
            }
        }

        // update finishLine
        finishLine += nebVel * deltaTime;

        if (collision)
        {
            DrawText("Game Over!", windowDimensions[0] / 4.0f, windowDimensions[1] / 2.0f, 40, RED);
        }
        else if (scarfyData.pos.x > finishLine)
        {
            DrawText("You Win!", windowDimensions[0] / 4.0f, windowDimensions[1] / 2.0f, 40, GREEN);
        }
        else
        {
            for (AnimData nebData : nebulae)
            {
                // draw nebulae
                DrawTextureRec(nebula, nebData.rec, nebData.pos, nebData.tint);
            }
            // draw scarfy
            DrawTextureRec(scarfy, scarfyData.rec, scarfyData.pos, WHITE);
        }

        // end drawing
        EndDrawing();
    }

    UnloadTexture(scarfy);
    UnloadTexture(nebula);
    UnloadTexture(background);
    UnloadTexture(midground);
    UnloadTexture(foreground);
    CloseWindow();
}